/**
* fureweb( https://bitbucket.org/fureweb_js/slotmachine )
*/
;
(() => {
  //디버그 모드
  const _debug = false;

  //이벤트 리스너 장착
  document.getElementById("gameStart").onclick = (e) => {
      gameStart();
  }

  //통계를 위한 변수
  let statistics = {
      0: 0, //total
      1: 0, //1등
      2: 0, //2등
      3: 0, //3등
      4: 0 //4등
  };

  let currentTurn = 0; //현재 turn수
  let seedMoney = 1000; //최초 부여되는 시드머니
  let slot1, slot2, slot3; //배열 대신 각각의 슬롯 고유번호에 맞는 변수 선언. for문으로 순회하는 것이 더 낫나? 아오.. 나중에 보니 배열이 더 나은거같다.. 그건 나중에하자.
  let grade, ratio;

  const betMoney = 100; //배팅 시 사용되는 금액. 다음에는 이 돈을 증액시킬 수 있게 리팩토링

  const isWin = _ => { //몇등에 당첨되었는지, 돌려준다. false가 돌아오면 꽝으로 그냥 return으로 잔액을 증가시키지 않고 그 외인 경우에는 등수에 맞게 처리한다.
      let result = false;

      //현재의 slot들에 들어있는 번호를 확인하여, 당첨 조건에 일치하는지 확인한다.
      if (isAllSameNumbers()) {
          //현재 slot에 박힌 번호를 본다.
          result = true;
          grade = slot1 === 7 ? 1 : slot1 === 6 ? 2 : 3; //7이면 1등, 6이면 2등, 그외엔 3등(7과 6이 아니면서 셋 다 같은 번호)
          ratio = grade === 1 ? 100 : grade === 2 ? 10 : 2; //ratio는 100 / 10 / 2배로 수정
      } else if (checkConsecutiveNumbers().result) {
          result = true;
          grade = 4;
          ratio = 1;
      }
      return {
          result: result,
          grade: grade,
          ratio: ratio
      };
  };

  const isAllSameNumbers = _ => slot1 === slot2 && slot1 === slot3; //번호 3개를 비교한 결과를 돌려준다.

  const checkConsecutiveNumbers = _ => {
      let number = undefined;
      const result = slot1 === slot2 || slot2 === slot3;

      if (result) //연속된 번호였다면 숫자가 들어가고, 아니었다면 undefined상태로 number가 채워진다.
          number = slot1 === slot2 ? slot1 : slot3;

      //object로 리턴
      return {
          result: result,
          number: number
      };
  }


  const manipulateMoney = (operator, amount, ratio) => {

      //유효하면 처리
      switch (operator) {
          case "+":
              seedMoney += amount * ratio;
              break;
          case "-":
              seedMoney -= amount;
              break;
          default:
              ban(); //그 외에는 밴시킨다.
              break;
      }

  }
  const updateStatisticsView = (v = {
      result: false
  }) => {

      //당첨된 경우에 result가 true로 넘어오고, 몇등인지 알 수 있다.
      if (v.result) {
          statistics[grade] += 1;
      }

      document.getElementById("totalCount").innerHTML = statistics[0];
      document.getElementById("first").innerHTML = statistics[1];
      document.getElementById("second").innerHTML = statistics[2];
      document.getElementById("third").innerHTML = statistics[3];
      document.getElementById("fourth").innerHTML = statistics[4];

      const p1 = Math.ceil(statistics[1] / statistics[0] * 100);
      const p2 = Math.ceil(statistics[2] / statistics[0] * 100);
      const p3 = Math.ceil(statistics[3] / statistics[0] * 100);
      const p4 = Math.ceil(statistics[4] / statistics[0] * 100);


      if (p1 > 0) document.getElementById("firstPercent").innerHTML = p1;
      if (p2 > 0) document.getElementById("secondPercent").innerHTML = p2;
      if (p3 > 0) document.getElementById("thirdPercent").innerHTML = p3;
      if (p4 > 0) document.getElementById("fourthPercent").innerHTML = p4;


  };

  const updateResultView = (option = {
      type: ''
  }) => {

      //적용된 시드머니를 출력시킨다.
      document.getElementById("seedMoney").innerHTML = seedMoney;
      const resultDOM = document.getElementById("result");

      if (option.type === "reset") {
        if( _debug ) console.log("파산");
        else alert("당신은 파산하였습니다. 확인을 누르면 게임을 다시 시작합니다...");

        seedMoney = 1000;
        currentTurn = 0;
        document.getElementById("seedMoney").innerHTML = seedMoney;
        document.getElementById("result").innerHTML = "";
        document.getElementById("slot1").innerHTML = "";
        document.getElementById("slot2").innerHTML = "";
        document.getElementById("slot3").innerHTML = "";
        return;
      } else if (option.type === "won") {
          const resultMessage = `${currentTurn+1} 회 : ${grade}등 당첨! (${betMoney*ratio}원 추가)<br/>`;
          resultDOM.innerHTML = resultMessage + resultDOM.innerHTML;
          //updateCount 이곳에서 증가
          updateCount();
      } else if (option.type === "lose") {
          const resultMessage = `${currentTurn+1} 회 : 꽝!<br/>`;
          resultDOM.innerHTML = resultMessage + resultDOM.innerHTML;
          //updateCount 이곳에서 증가
          updateCount();
      }
  };

  const updateCount = _ => {
      //턴 수를 증가시킨다.
      currentTurn += 1;
      statistics[0] += 1;

      //전체 누적 횟수 증가 처리
      // let maxLife = document.getElementById("maxLife").innerHTML;
      // maxLife = parseInt(maxLife) + 1;
      // if( currentTurn > parseInt(document.getElementById("maxLife").innerHTML) )
      //   maxLife = currentTurn;
  };

  const rollIt = _ => {
      manipulateMoney("-", 100); //시작 머니 차감
      slot1 = pickRandomNumber();
      document.getElementById("slot1").innerHTML = slot1;
      slot2 = pickRandomNumber();
      document.getElementById("slot2").innerHTML = slot2;
      slot3 = pickRandomNumber();
      document.getElementById("slot3").innerHTML = slot3;
  };

  const pickRandomNumber = _ => Math.ceil(Math.random() * 7);

  const isThereSeedMoney = _ => seedMoney >= betMoney; //bet money보다 더 많이 있어야 굴릴 수 있으니.

  //메인 실행함수
  const gameStart = _ => {
      isThereSeedMoney();
      rollIt();

      const result = isWin();

      if (isWin().result) {
          updateResultView({
              type: "won"
          });
          updateStatisticsView(result);
          manipulateMoney("+", betMoney, result.ratio);

      } else {
          updateResultView({
              type: "lose"
          });
      }

      updateResultView();
      updateStatisticsView();

      checkSeedMoney();
  }

  const checkSeedMoney = _ => {
      if (seedMoney === 0) {
          const option = {
              type: "reset"
          };
          updateResultView(option);
      }
  };
  if(_debug)
    for(let i=0; i<1000; i++)
     gameStart();
})();
