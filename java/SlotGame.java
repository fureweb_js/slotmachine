/**
* 제작자 : 이서연(sps1122@naver.com)
* 제작일 : 2016년 10월 8일
*/

import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;

public class SlotGame {

	public char slotNum[] = {'A','1','3','4','5','6','7','8','9','K'};
	public HashMap<String, Integer> winNum = new HashMap<>();
	private int seedMoney = 1000;


	public SlotGame(){
		winNum.put("777", 4);
		winNum.put("AAA", 3);
		winNum.put("KKK", 2);
		winNum.put("77A", 1);
		winNum.put("7A7", 1);
		winNum.put("A77", 1);
		winNum.put("77K", 1);
		winNum.put("7K7", 1);
		winNum.put("K77", 1);
	}

	public String selectNum(int roNum){
		StringBuffer strBuffer = new StringBuffer();

		int ranNum = 0;
		for(int i=1; i <4 ; i++){
			ranNum = (int)(Math.random() * 10 + 1);
			ranNum = (ranNum * roNum)%10;

			strBuffer.append(slotNum[ranNum]);
		}

		System.out.println("나온 숫자는 : " + strBuffer);

		return strBuffer.toString();
	}

	public int seedCal(String selectNum, int seedMoney, int batting){
		int value = 0;

		if(winNum.get(selectNum) != null)
			value = winNum.get(selectNum);

		int winMoney = batting*value - batting;

		System.out.println("당청금 :" + winMoney);

		return seedMoney + winMoney;
	}


	public static void main(String[] args) {
		SlotGame slotGame = new SlotGame();
		Scanner scanner = new Scanner(System.in);

		int batting =0;
		int roNum =0;
		while(slotGame.seedMoney>0){
			System.out.println("현재 소지금은 " + slotGame.seedMoney + "원 입니다.");

			System.out.print("이번 게임에 거실 금액을 입력 : ");
			batting = scanner.nextInt();

			System.out.println("슬롯 회전 횟수를 입력 : ");
			roNum = scanner.nextInt();

			slotGame.seedMoney = slotGame.seedCal(slotGame.selectNum(roNum), slotGame.seedMoney, batting);

			System.out.println("현재 남은 금액 : " + slotGame.seedMoney);

		}

		System.out.println("당신은 파산하였습니다.");
	}

}
